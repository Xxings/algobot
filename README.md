# Algobot

##This Project Slack chatbot Project

- 이름 : 알고리즘 뽀개기  / EN : Hey, Algo / 알.뽀

- 대상 : 알고리즘 준비하고싶은 사람을 위한 봇
- 내용 : SW expert에서 하루에 문제를 랜덤으로 제공해서, 알고리즘 공부를 열심히 하게함
- 기능 : slack channel 봇으로 사용자들이 참여하면,
1. 아침 9시에 문제 뽑아주기
2. 매 시각 정각마다, 랭킹과 안푼 사람 알려주기
3. 저녁 24시에 안뽑은 사람 알리기

<hr>

