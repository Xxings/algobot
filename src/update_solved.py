'''
import
'''

from selenium import webdriver  # selenium
from bs4 import BeautifulSoup   # Crwaling
import urllib.request
import ast  # txt <-> list

import random

# load _setting
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname("_setting"))))
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname("setting"))))
import _setting as _SET
import setting as SET

from datetime import datetime   #현재 날짜 넣기

driver = ''
# driver = webdriver.Chrome(SET.DRIVER_PATH)  # 드라이버 켜기
'''
login : 세션 권한 취득
input : <none>
output : <bool>
'''
def login():
    try:
        driver.get(SET.LOGIN_URL)
        # id/pw input값 입력
        driver.find_element_by_id('id').send_keys(SET.SECRET_SW_EXPERT_ID)
        driver.find_element_by_id('pwd').send_keys(SET.SECRET_SW_EXPERT_PW)
        driver.find_element_by_xpath('//*[@id="LoginForm"]/div/div/div[2]/div/div/fieldset/div/div[4]/button').click()

    except Exception as ex:
        print('login() 에러가 발생 했습니다', ex)
        return False
    return True

'''
readText
INPUT : <str>
OUTPUT : <list> 
 [
     ['안경무_0240413', 'AWhlKiOKPI0DFAXp',
            [
            {'pro_no': 6719, 'solved': '', 'date': '20190621'},
            {'pro_no': 0, 'solved': '', 'date': ''}
            ]
     ],
     ['심형관_0230413', 'AWhlKiOKPI0DFAXp',
            [
            {'pro_no': 0, 'solved': '', 'date': ''},
            {'pro_no': 0, 'solved': '', 'date': ''}
            ]
     ],
 ]
'''
def readPeople(src):
    with open(src, encoding='utf8') as file:
        data = file.read()
        print(data)
        if data:
            people_data = ast.literal_eval(data)   # string -> list
        else:
            people_data = []
            print("사용자가 없습니다.")
    return people_data

'''
readText
INPUT : <str>
OUTPUT : <list>
'''
def readText(src):
    with open(src, encoding='utf8') as file:
        data = file.read()
        print(data)
        if data:
            contents = ast.literal_eval(data)   # string -> list
        else:
            contents = []
            print("DB가 없으므로 초기상태로 진행합니다.")
    return contents

'''
getToday : 저장할때 쓸지몰라 따로 빼놓은 날짜형식 
input : <none>
output : <str>
'''
def getToday():
    now = datetime.now()
    month = now.month if now.month > 10 else str(0)+str(now.month)
    today = ('%s%s%s' % (now.year, month, now.day))
    return today


'''
todayProblem
input : <list> <str>
output : <int>
'''
def todayProblem(contents, date):
    problem_no = None

    for dict_data in contents :
        if dict_data['date'] == date:
            problem_no = dict_data['no']
            break


    return problem_no

'''
is_solved
input : <list>,no
output : <bool>
'''
def is_solved(people_data, no):
    print("문제번호" + str(no))
    driver.get(SET.LOGIN_URL)
    for data in people_data:
        id = data[0]
        encoder_id = data[1]
        print(encoder_id)
        targetUrl = SET.TARGET_URL + encoder_id
        try:
            driver.get(targetUrl)  # 크롤링 시작
            html=driver.page_source
            soup = BeautifulSoup(html, 'html.parser')
            # print(soup)
            div_soup = soup.find('div', class_="solvingclub")
            # print(type(div_soup))
            solve_nos = div_soup.find_all('span', class_="week_num")
            solve_dates = div_soup.find_all('span', class_="code-sub-date")

            for number, date in zip(solve_nos, solve_dates):
                pro_no = int(number.get_text().replace('.', ''))
                pro_date = date.get_text().split()
                pro_day = pro_date[0].replace('-', '')
                pro_time = pro_date[1].replace(':', '')

            ## 정식 BOT
                if str(pro_date) < str(getToday()):   #해당 일자가없어서 찾을 필요 없는경우
                    break
                    # 해당 일자 같은경우 <str> = <str>
                    # 해당 문제 번호 같을경우 <int> = <int>
                elif str(pro_day) == str(getToday()) \
                        and pro_no == no:
                    print("풀었구나")
            ## 일자 뺀경우 TESTCODE
                # if pro_no == no:
                #     updateUser(people_data, no, data)
                #     insertRank(id,pro_time)

        except Exception as ex:
            print(encoder_id+' 사용자의 긁어오기 실패', ex)
            return False
    return True
'''
updateUser
input : <list><int><list>
output : <bool>
'''
def updateUser(people_data, no, targetPeople):
    try:
         # ['심형관_0230413', 'AWhlKiOKPI0DFA1p',
         #            [
         #            {'pro_no': 0, 'solved': '', 'date': ''},{'pro_no': 6719, 'solved': True, 'date': '20190711'}
         #            ]
        data = {'pro_no' : no,
                'solved' : True,
                'date' : getToday()
                }
        targetPeople[2].append(data)

        print(targetPeople)

        fw = open(SET.USER_DB, 'w+t', encoding='utf8')
        fw.write(str(people_data))

        print(people_data)

    except Exception as ex:
        print(' 사용자의 기록 업데이트 실패', ex)
        return False

    return True

'''
insertRank
input : <str><str>
output : <bool>
'''
def readRank():
    RANK_DB = './db/' + getToday() + ".txt"
    fr = open(RANK_DB, 'r', encoding='utf8')
    data = fr.read()
    if data:
        contents = ast.literal_eval(data)
    else:
        contents = []
    return contents

def insertRank(name,time):
    RANK_DB = './db/' + getToday() + ".txt"
    try :
        dataSet = readRank()
        with open(RANK_DB, 'w+t', encoding='utf8') as fw:
            dataSet.append([name, time])
            fw.write(str(dataSet))

    except Exception as ex:
        print(' 사용자의 랭킹 기록 업데이트 실패', ex)
        return False

    return True
'''
viewRank

output : <str>
'''
def viewRank():
    SCORE_SET = [300,250,200,200,200,100]
    dataSet = readRank()
    dataSet.sort(key=lambda element: element[1])
    contents = ' = 오늘의 랭킹 = \n'
    rank = 1
    for name, time in dataSet:
        contents += ('%s : %s (%s:%s)  SCROE : %i \n' % (rank, name, time[:2], time[2:], SCORE_SET[rank-1] if rank<=5 else SCORE_SET[5]))
        rank += 1
    print("/RANK : \n"+contents)
    return contents

def updateRank():
    login()
    user_list = readPeople(SET.USER_DB)
    db_data = readText(SET.PROBLEM_DB)
    no = todayProblem(db_data, getToday())
    # print(user_list)
    # print(db_data)
    # print(no)
    is_solved(user_list, no)
    # updateUser(user_list,30,user_list[1])
    # insertRank('심형관','0930')

