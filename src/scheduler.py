# # 사용방법
# import schedule
# import time
# from rank import #함수
# from unsolved import#함수
# from problem import #함수
#
# # 매 시간 실행 .. rank
# schedule.every().hour.do(rank.함수)
# # 매일 10:00 에 실행 .. problem
# schedule.every().day.at("10:00").do(problem.함수)
# # 매일 24:00 에 실행 . unsolved
# schedule.every().day.at("24:00").do(unsolved.함수)
#
# while True:
#     schedule.run_pending()
#     time.sleep(1)
#
