# #-*- coding: utf-8 -*-
# '''
# src/Problem_Create::<none>/<none> : 대상 사이트에서 문제를 긁어오는 메소드
# 대상 사이트 : https://swexpertacademy.com/main/code/problem/problemList.do
# '''
# import urllib.request
# from bs4 import BeautifulSoup
# import re  # title 문자열 정리
# import csv  # Problem List 정리
#
# TARGET_SITE = "https://swexpertacademy.com/main/code/problem/problemList.do?problemLevel=1&problemLevel=2&problemLevel=3&problemLevel=4&problemLevel=5&problemLevel=6&problemLevel=7&problemLevel=8&problemTitle=&orderBy=FIRST_REG_DATETIME&select-1=&pageSize=10&pageIndex="
# PROBLEM_LIST = { i : "probelmLevel="+str(i) for i in range(1,9) }
# '''
#
# INPUT : <NONE>
# OUTPUT : <BOOL>
# '''
#
#
# def main():
#     print(PROBLEM_LIST)
#
#     for i in range(1, 2):
#         # 문자열 연산
#         url = TARGET_SITE + str(i)
#         source_code = urllib.request.urlopen(url).read()
#         soup = BeautifulSoup(source_code, "html.parser")
#
#         problem_List = soup.find("div", class_="widget-list")
#         problem_nos = problem_List.find_all("span", class_="week_num")
#         problem_titles = problem_List.find_all("span", class_="week_text")
#         problem_levels = problem_List.find_all("span", class_="badge")
#         problem_dates = None
#
#         dataset = []
#
#         for no, title, level in zip(problem_nos, problem_titles, problem_levels):
#             dataset.append(
#                 {"no": no.get_text(), "title": cleanText(title.get_text()), "level": level.get_text(), "date": None})
#
#     print(dataset)
#     # if csv_write(dataset):
#     #     return True
#     # else:
#     #     return False
#
#
# '''
# cleanText : 텍스트에 포함되어 있는 특수 문자 제거
# INPUT : <str>
# OUTPUT : <str>
# '''
# def cleanText(readData):
#
#     del_num = readData.find('[')
#     print(readData)
#     print(del_num)
#     if del_num != -1:
#         readData = readData[:del_num - 1]
#
#     text = re.sub('[-=+,#/\?:^$.@*\"※~&%ㆍ!』\\‘|\(\)\[\]\<\>`\'…》|/\t|/\r\n|\r|\n/]', '', readData)
#
#     return text
#
#
# '''
# csv_write
# INPUT : <list>
# OUTPUT : <bool>
# '''
#
#
# def csv_write(readData):
#     try:
#         f = open('ProblemList.csv', 'w', encoding='utf-8', newline='')
#         wr = csv.writer(f)
#         for data in readData:
#             wr.writerow(data)
#             f.close()
#
#     except Exception as ex:
#         f.close()
#         print('에러가 발생 했습니다', ex)
#         return False
#     return True
#
#
# if __name__ == "__main__":
#     main()
