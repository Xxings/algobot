# -*- coding: utf-8 -*-
import ast

# load _setting
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname("_setting"))))
import _setting as _SET
import setting as SET

def readText(src):
    fr = open(src, 'r', encoding='utf8')
    data = fr.read()
    if data:
        contents = ast.literal_eval(data)  # string -> list
    else:
        contents = []
        print("DB가 없으므로 초기상태로 진행합니다.")

    fr.close()
    return contents

def writeText(dict,list):
    fw = open(SET.USER_DB, 'w+t', encoding='utf8')
    print(dict)
    dataSet = list + [dict]
    fw.write(str(dataSet))

def deleteWriteText(list):
    fw = open(SET.USER_DB, 'w+t', encoding='utf8')
    fw.write(str(list))

'''
input : <str><str>
'''
def addUser(userId, encodingId):
    userList = [userId, encodingId, [{'pro_no': 0, 'solved': '', 'date': ''}]]
    writeText(userList, readText(SET.USER_DB))
    print("/ADD : 알린이 님의 User ID는 " + userId + "이고, Encoding ID는" + encodingId + "입니다.\n")
    return '알린이 님의 User ID는 ' + userId + '이고, Encoding ID는' + encodingId + '입니다.\n'

'''
input : <str>
'''
def delUser(userId):
    userList = readText(SET.USER_DB)
    for i in range(0, len(userList)):
        if userList[i][0] == userId:
            del userList[i]
            break
    deleteWriteText(userList)
    print("/DEL : "+userId+'님이 삭제되었습니다.')
    return userId+'님이 삭제되었습니다.\n'

def viewList():
    data =readText(SET.USER_DB)
    print(data)
    contents = ' = 참여자 명단 = \n'
    print("/LIST")
    for people in data:
        contents += '%s \t'%(people[0])

    return contents