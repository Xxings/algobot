from .update_solved import *

UNSOLVED_DB = './db/' + getToday()+'_unsolved.txt'

def unsolved_solved(people_data, no):  # userinfo / 문제번호
    print("문제번호" + str(no))
    today = str(getToday())
    unSolvedList = []
    for data in people_data:
        for Userproblem in data[2]:
            print(Userproblem['pro_no'])
            print(Userproblem['date'])
            if Userproblem['pro_no'] == no and Userproblem['date'] == today:
                break
            else:
                unSolvedList.append(data[0])

    print(unSolvedList)
    return unSolvedList



def insertUnsolved():
    db_data = readText(SET.PROBLEM_DB)
    UNSOLVED_DB = './db/' + getToday()+'_unsolved.txt'
    people_data = readPeople(SET.USER_DB)
    try:
        unSolvedList = unsolved_solved(people_data,todayProblem(db_data, getToday()))


        with open(UNSOLVED_DB, 'w+t', encoding='utf8') as fw:
            fw.write(str(unSolvedList))

    except Exception as ex:
        print('안 푼 사람 목록 업데이트 실패', ex)
        return '안 푼 사람 목록 업데이트 실패'

    return "최신정보로 업데이트 되었습니다."

def readUnsolved():
    UNSOLVED_DB = './db/' + getToday()+'_unsolved.txt'
    fr = open(UNSOLVED_DB, 'r', encoding='utf8')
    data = fr.read()
    if data:
        print(repr(data))
        contents = ast.literal_eval(data)
    else:
        contents = []
    return contents

def viewUnsolved():
    print('ViewUnsolved')
    contents = ' = 금일 문제풀이 안한 리스트 = \n'
    dataSet = readUnsolved()
    for name in dataSet:
        contents += (' \t %s ' %(name))

    print("/UNSOLVED : " + contents)
    return contents