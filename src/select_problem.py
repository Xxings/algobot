#-*- coding: utf-8 -*-
'''
src/select_problem::<none>/<dict> : 대상 사이트에서 문제를 긁어오는 메소드
대상 사이트 : https://swexpertacademy.com/main/code/problem/problemList.do
'''

import urllib.request
from bs4 import BeautifulSoup
import re  # title 문자열 정리

import ast  # txt <-> list
from datetime import datetime   #현재 날짜 넣기

import random
# load _setting
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname("_setting"))))
import _setting as _SET
import setting as SET

# import csv  # Problem List 정리

TARGET_SITE = "https://swexpertacademy.com/main/code/problem/problemList.do?pageSize=30&pageIndex="
PROBLEM_LIST = { i : "problemLevel="+str(i) for i in range(0,9) }
SITE_PAGE_ITEM = [1]

'''
searchProblem
INPUT : <NONE>
OUTPUT : <list>
'''
def searchProblem():
    # print(PROBLEM_LIST)

    ##수정할것
    rand = random.randrange(1,7)
    # 문자열 연산
    url = TARGET_SITE + str(rand)
    for level in range(1,9):
        if SET.SET_LEVELS[level] == True :
            url += "&"+PROBLEM_LIST[level]

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    problem_List = soup.find("div", class_="widget-list")
    problem_nos = problem_List.find_all("span", class_="week_num")
    problem_titles = problem_List.find_all("span", class_="week_text")
    problem_levels = problem_List.find_all("span", class_="badge")
    problem_dates = getToday()

    dataset = []

    for no, title, level in zip(problem_nos, problem_titles, problem_levels):
        dataset.append(
            {"no": int(no.get_text().replace('.','')), "title": cleanText(title.get_text()), "level": level.get_text(), "date": problem_dates}
        )

    return dataset
'''
getToday : 저장할때 쓸지몰라 따로 빼놓은 날짜형식 
input : <none>
output : <str>
'''
def getToday():
    now = datetime.now()
    month = now.month if now.month > 10 else str(0)+str(now.month)
    today = ('%s%s%s' % (now.year, month, now.day))
    return today

'''
cleanText : 텍스트에 포함되어 있는 특수 문자 제거
INPUT : <str>
OUTPUT : <str>
'''
def cleanText(readData):

    del_num = readData.find('[')
    # print(readData)
    # print(del_num)
    if del_num != -1:
        readData = readData[:del_num - 1]

    text = re.sub('[-=+,#/\?:^$.@*\"※~&%ㆍ!』\\‘|\(\)\[\]\<\>`\'…》|/\t|/\r\n|\r|\n/]', '', readData)

    return text


'''
readText
INPUT : <str>
OUTPUT : <list>
'''
def readText(src):
    fr = open(src, 'r', encoding="UTF8")
    data = fr.read()
    if data:
        print(repr(data))
        contents = ast.literal_eval(data)  # string -> list
        # contents = ast.literal_eval("[{'no':7967, 'title':'감시 로봇', 'level':'6','date':'20190712'}]")
    else:
        contents = []
        print("DB가 없으므로 초기상태로 진행합니다.")

    fr.close()
    return contents

'''
is_alreayProblem
INPUT : <file><int>
OUTPUT : <BOOL>
'''
def is_alreayProblem(contents,no):

    flag_is = False              #있는지 확인해주는 flag

    for dict_data in contents :
        if dict_data['no'] == no:
            flag_is = True

    return flag_is
'''
wrtieText
INPUT : <dic> <list<dict>>
OUTPUT : <BOOL>
'''
def writeText(dict,list):
    try:
        dataSet = list + [dict]
        with open(SET.PROBLEM_DB, 'w+t',encoding='utf8') as file:
            file.write(str(dataSet))

    except Exception as ex:
        print('에러가 발생 했습니다', ex)
        return False
    return True

def createProblem():
    scarp_data = searchProblem()
    print(SET.PROBLEM_DB)
    db_data = readText(SET.PROBLEM_DB)
    for data in db_data:
        if data['date'] == getToday():
            print("오늘 문제는 이미 등록되어있습니다. 문제번호 :"+str(data['no']))
            return ('오늘 문제는 이미 등록되어있습니다. 문제번호 : %s'%(str(data['no'])))

    random.shuffle(scarp_data)
    for data in scarp_data:
        if not is_alreayProblem(db_data, data['no']):
            commit_data = data
            break

    writeText(commit_data, db_data)
    print(str(commit_data['no'])+"번 문제가 추가되었습니다.")
    return ("%s 번 문제가 추가되었습니다."%(str(commit_data['no'])))

def force_createProblem():
    scarp_data = searchProblem()
    print(SET.PROBLEM_DB)
    db_data = readText(SET.PROBLEM_DB)

    random.shuffle(scarp_data)
    for data in scarp_data:
        if not is_alreayProblem(db_data, data['no']):
            commit_data = data
            break
    writeText(commit_data, db_data)
    print(str(commit_data['no'])+"번 문제가 추가되었습니다.")
    return ("%s 번 문제가 추가되었습니다."%(str(commit_data['no'])))

def viewProblem():
    db_data = readText(SET.PROBLEM_DB)
    for data in db_data:
        if data['date'] == getToday():
            return ('오늘 문제는, \n * 문제번호 \t: %s \n * 문제이름 \t: %s \n * 레벨 \t\t\t: %s \n'
                    % (str(data['no']), str(data['title']), str(data['level']))
                    )
    return "오늘의 문제가 아직 생성되지 않았습니다."

def viewSettings():
    setLevel=SET.SET_LEVELS
    contents = " = 세팅 VIEWER = \n"
    contents += viewProblem()
    contents += " = 난이도 설정 = \n"
    for loop in range(1,9):
        contents += ("%s : %s \n"%('D'+str(loop), 'TRUE' if setLevel[loop] else 'FALSE'))

    return contents