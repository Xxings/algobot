#드라이버 PATH 설정
DRIVER_PATH = "./src/driver/chromedriver.exe"
LOGIN_URL = "https://swexpertacademy.com/main/identity/anonymous/loginPage.do"
TARGET_URL = "https://swexpertacademy.com/main/userpage/code/userCode.do?userId="

# [['안경무','AWhlKiOKPI0DFAXp',[{}]]]
USER_DB = "./db/USER_INFO.txt"
PROBLEM_DB = "./db/PROBLEM_DB.txt"

##문제난이도
SET_LEVELS= {
    1 : False,
    2 : False,
    3 : False,
    4 : True,
    5 : True,
    6 : True,
    7 : True,
    8 : True
}