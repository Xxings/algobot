# -*- coding: utf-8 -*-
# import re
# import urllib.request
#
# from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
# import ast

import _setting as _SET
# import setting as SET

import sys

sys.path.append("/src/")
from src.add_del import addUser, delUser, viewList
from src.update_solved import viewRank
from src.select_problem import createProblem, viewProblem, force_createProblem, viewSettings
from src.unsolved_user import viewUnsolved, insertUnsolved

#스케쥴
import schedule
import time

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(_SET.SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=_SET.SLACK_TOKEN)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    chk = text.split()
    # print(event_data)
    # print(type(chk))
    # print(len(chk))
    command = str(chk[1]).upper()
    keywords = '기본값'
    if(command=='ADD'):
        if len(chk) > 3:
            keywords = addUser(chk[2], chk[3])
        else :
            keywords = 'User ID와 Encoding ID를 입력해주세요. (예: ADD 김땡땡, AWuHFASqKN8DFAW4)\n'

    elif(command=='DEL'):
        if len(chk) > 2:
            keywords = delUser(chk[2])
        else:
            keywords = 'User ID와 Encoding ID를 입력해주세요. (예: DEL 김땡땡)\n'
    elif(command=='LIST'):
        keywords = viewList()

    elif(command=='RANK'):
        keywords = viewRank()

    elif(command=='PROBLEM'):
        keywords = viewProblem()

    elif(command=="UNSOLVED"):
        keywords = viewUnsolved()

    # 스케쥴러 대신에하는 시연용
    elif(command=="ADMINCREATE"):
        keywords = createProblem()

    elif(command=="ADMINUPDATE"):
        keywords = insertUnsolved()

    elif(command=="ADMINFORCECREATE"):
        keywords = force_createProblem()

    elif(command=="ADMINHELP"):
        keywords = "\t= 시연용 해당 데이터 = \n" \
                   "송제민 AWcW24Gap4IDFARf"

    elif(command=="VIEWSETTING"):
        keywords = viewSettings()

    elif(command=="HELP"):
        keywords = "\t= 명령어 목록 = \n" \
                   "VIEWSETTING : 현재 SETTING을 보여줍니다. \n" \
                   "ADD {사용자이름} {사용자인코딩}: 알.뽀에 참여합니다. \n" \
                   "ADMINHELP : (시연용) ADD 할 미리뽑아둔 추가 데이터 \n" \
                   "LIST : 알.뽀 참가인원을 호출합니다. \n" \
                   "DEL {사용자이름} : 알.뽀에서 탈퇴합니다. \n" \
                   "UNSOLVED : 아직 안푼사람을 출력합니다. \n" \
                   "ADMINUPDATE : (스케쥴용)풀은 정보를 크롤링하여 정보를 업데이트합니다. \n" \
                   "RANK : 현재 푼 사람과 랭크를 확인합니다. \n" \
                   "PROBLEM : 오늘의 문제를 출력합니다.\n" \
                   "ADMINCREATE : (스케쥴용)오늘의 문제를 생성합니다.\n" \
                   "ADMINFORCECREATE : (시연용)오늘의 문제를 강제로 더 생성합니다. \n" \
                   "HELP : 도움말을 출력합니다.\n"

    else:
        keywords = '잘못된 명령어입니다. (참고 : HELP)'

    slack_web_client.chat_postMessage(
        channel=channel,
        text=keywords
    )


def scheduler():
    # event_data['text']='<@UKY7F2RR9> rank'

    schedule.every().day.at("09:00").do(createProblem())
    # 해당출력
    schedule.every().hour.do(viewRank())
    # unsolved 호출
    schedule.every().day.at("24:00").do(viewUnsolved())

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
   return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    scheduler()
    while True:
        schedule.run_pending()
        time.sleep(1)
    app.run('0.0.0.0', port=5000)
    # app.run('0.0.0.0', port=5000, debug=True)